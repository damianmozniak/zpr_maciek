#ifndef CSTRING_EXCEPTION_HPP_INCLUDED
#define CSTRING_EXCEPTION_HPP_INCLUDED
#include<exception>
struct cstring_exception : std::exception // klasa wyjatku o podanym opisie (podanym jako ciag znakow)
{
    const char* what_;
    cstring_exception(const char* w);
    const char* what() const noexcept;
};
#endif // CSTRING_EXCEPTION_HPP_INCLUDED
