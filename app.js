var Player = require('./js/Player.js');

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
users = [];
connections = [];

app.use(express.static(__dirname + '/'));

server.listen(process.env.PORT || 8000);
console.log('Server running......');

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

var COLORS = {
    1: "BLUE",
    2: "RED",
    3: "YELLOW",
    4: "GREEN"
};

var players;
players = [];

var gameStarted = false;

var playerTurn = 0;

var rolledNumber = 0;

var pawns = [];

var board = [
    [21, 21, 0, 0, 1, 1, 1, 0, 0, 22, 22],
    [21, 21, 0, 0, 1, 32, 1, 0, 0, 22, 22],
    [0, 0, 0, 0, 1, 32, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 32, 1, 0, 0, 0, 0],
    [1, 1, 1, 1, 1, 32, 1, 1, 1, 1, 1],
    [1, 31, 31, 31, 31, 0, 33, 33, 33, 33, 1],
    [1, 1, 1, 1, 1, 34, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 1, 34, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 34, 1, 0, 0, 0, 0],
    [23, 23, 0, 0, 1, 3, 1, 0, 0, 24, 24],
    [23, 23, 0, 0, 1, 1, 1, 0, 0, 24, 24]
];

io.sockets.on('connection', function (socket) {

    connections.push(socket);
    console.log('Connected: %s sockets connected', connections.length);

    socket.on('disconnect', function (data) {
        //Disconnect
        connections.splice(connections.indexOf(socket), 1);
        console.log('Disconnected: %s sockets connected', connections.length);
    });

    var ip = socket.handshake.address;

    socket.on('setPlayerData', function (color, callback) {
        callback(setPlayerData(color, ip));
    });

    socket.on('joinPlayer', function () {
        (function () {
            if (!playerInGame(ip)) {
                console.log("Player joined");
                var player = new Player(ip);
                players.push(player);
                socket.broadcast.emit('playerJoined', {players: players.length});
            }
        })();
    });

    socket.on('startGame', function () {
        startGame(socket);
    });

    //STARTOWE INFO O GRZE CO SIE DZIEJE
    socket.on('getGameInfo', function (data, callback) {
        callback(gameStarted, playerInGame(ip), players.length, getPlayerObject(ip));
    });

    socket.on('getPlayerObject', function (data, callback) {
        callback(getPlayerObject(ip));
    });

    socket.on('playerInGame', function (data, callback) {
        callback(playerInGame(ip));
    });

    socket.on('getPawns', function (data, callback) {
        callback(getPawns());
    });

    socket.on('getRolledNumber', function (data, callback) {
        callback(rolledNumber);
    });

    socket.on('colorIsAvailable', function (data, callback) {
        callback(colorIsAvailable(data));
    });

    socket.on('isPlayerTurn', function (data, callback) {
        callback(isPlayerTurn(ip));
    });

    socket.on('movePawn', function (pickedPawnPos, callback) {
        callback(movePawn(pickedPawnPos, ip));
    });

    socket.on('rollTheDice', function (data, callback) {
        callback(rollTheDice(ip));
    });

    socket.on('getPawnPath', function (id, callback) {
        var pawn = getPawnById(id);
        var player = getPlayerObject(ip);

        callback(findPath(pawn, player.StartPoint));
    });
});

function movePawn(pickedPawnPos, ip) {
    var player = getPlayerObject(ip);
    var pickedPawn = board[pickedPawnPos[0]][pickedPawnPos[1]];
    var callbackObj = {};

    //WYJSCIE PIONKIEM JEZELI JEST NA STARCIE
    if (pickedPawn.name === "PawnStart") {
        if (rolledNumber === 6) {
            //SPRAWDZANIE CZY COS STOI NA POLU STARTOWYM
            //JEZELI PUSTE (NA POLU JEST WPISANY INT 1 czyli puste) TO WYSTAW PIONKA
            if (typeof (board[player.StartPoint.x][player.StartPoint.y]) === "number") {
                (function(fcallback) {
                    //PRZYPISZ DO POLA STARTOWEGO PIONEK
                    board[player.StartPoint.x][player.StartPoint.y] = pickedPawn;

                    pickedPawn.name = "PawnBoard";

                    callbackObj.result = true;
                    callbackObj.pawn = "PawnStart";
                    callbackObj.desireX = player.StartPoint.x;
                    callbackObj.desireY = player.StartPoint.y;
                    callbackObj.pawnId = pickedPawn.id;

                    endMove();
                    io.emit('movePawn', callbackObj);
                    fcallback();
                })(function () {
                    //USTAWIA POPRZEDNIEMU POLU STARĄ WARTOSC
                    board[pickedPawn.boardPos[0]][pickedPawn.boardPos[1]] = parseInt("2" + player.color);

                    //PRZYPISZ NOWA POZYCJE
                    pickedPawn.boardPos[0] = player.StartPoint.x;
                    pickedPawn.boardPos[1] = player.StartPoint.y;
                });

            } else if (board[player.StartPoint.x][player.StartPoint.y].color === player.color) { //JEZELI STOI PIONEK GRACZA TO POWIADOM
                callbackObj.result = false;
                callbackObj.msg = "Na polu startowym stoi twój pionek.\nWybierz inny ruch!";
            } else { //TODO: JEZELI STOI PIONEK PRZECIWNIKA TO USUN I WSTAW SWOJ
                callbackObj.result = false;
                callbackObj.msg = "Stoi Pionek Przeciwnika";
            }
        } else {
            callbackObj.result = false;
            callbackObj.msg = "Aby wyjść pionkiem musisz wylosować 6";
        }
    } else if (pickedPawn.name === "PawnBoard" && rolledNumber !== 0) { //JEZELI PIONEK "W GRZE"

        var newDest = findPath(pickedPawn);

        if (board[newDest[0]][newDest[1]] === 1) {

            (function(fcallback) {
                board[newDest[0]][newDest[1]] = pickedPawn;

                //USTAW KIERUNEK
                pickedPawn.direction = newDest[2];

                callbackObj.result = true;
                callbackObj.pawn = "PawnBoard";
                callbackObj.desireX = newDest[0];
                callbackObj.desireY = newDest[1];
                callbackObj.pawnId = pickedPawn.id;

                endMove();
                io.emit('movePawn', callbackObj);
                fcallback();
            })(function(){
                //USTAWIA POPRZEDNIEMU POLU STARĄ WARTOSC
                board[pickedPawn.boardPos[0]][pickedPawn.boardPos[1]] = 1;
                //PRZYPISZ NOWA POZYCJE
                pickedPawn.boardPos[0] = newDest[0];
                pickedPawn.boardPos[1] = newDest[1];
            });

        } else {
            var pawnOnFiled = board[newDest[0]][newDest[1]];
            if (pawnOnFiled.color === player.color) {
                callbackObj.result = false;
                callbackObj.msg = "Na przeznaczonym polu stoi twój pionek.\nWybierz inny ruch!";
            } else {
                //BICIE PRZECIWNIKA !!!!!!!!!!!!
            }
        }
    }
    return callbackObj.msg;
}

(function addPieces() {
    var row, col, piece;

    for (row = 0; row < board.length; row++) {
        for (col = 0; col < board[row].length; col++) {
            if (String(board[row][col]).charAt(0) == '2') {
                if (row < 3) {
                    if (col < 3) {
                        piece = {
                            color: 1,
                            pos: [row, col]
                        };
                    } else {
                        piece = {
                            color: 2,
                            pos: [row, col]
                        };
                    }
                } else {
                    if (col < 3) {
                        piece = {
                            color: 3,
                            pos: [row, col]
                        };
                    } else {
                        piece = {
                            color: 4,
                            pos: [row, col]
                        };
                    }
                }
            } else {
                piece = 0;
            }

            if (piece) {
                var pieceObjGroup = {};

                switch (piece.color) {
                    case 1:
                        pieceObjGroup.color = 1;
                        pieceObjGroup.direction = 'N';
                        break;
                    case 2:
                        pieceObjGroup.color = 2;
                        pieceObjGroup.direction = 'E';
                        break;
                    case 3:
                        pieceObjGroup.color = 3;
                        pieceObjGroup.direction = 'W';
                        break;
                    case 4:
                        pieceObjGroup.color = 4;
                        pieceObjGroup.direction = 'S';
                        break;
                    default:
                        break;
                }

                pieceObjGroup.name = 'PawnStart';
                pieceObjGroup.boardPos = piece.pos;
                pieceObjGroup.startPos = piece.pos;
                pieceObjGroup.id = parseInt("" + piece.pos[0] + piece.pos[1]);

                pawns.push(pieceObjGroup);
                board[piece.pos[0]][piece.pos[1]] = pieceObjGroup;
            }
        }
    }
})();

function startGame(socket) {
    if (!gameStarted) {
        var everyonePickedColor = true;

        players.forEach(function (item) {
            if (item.color === null) {
                everyonePickedColor = false;
            }
        });

        if (everyonePickedColor) {
            gameStarted = true;
            io.emit('gameStarted', true);
            playerTurn = players[0].color;
            sendTurnInfo();
        } else {
            socket.emit('gameStarted', false);
        }
    }
}

function colorIsAvailable(data) {
    var available = true;

    players.forEach(function (item) {
        if (item.color === data.color) {
            available = false;
        }
    });
    return available;
}

function setPlayerData(color, ip) {
    var player = getPlayerObject(ip);
    var textMessage = "Wybrałeś pionki ";

    player.color = color;

    switch (color) {
        case 1:
            player.StartPoint.x = 4;
            player.StartPoint.y = 0;
            textMessage += "<span style='color:#5e6eff;'>Niebieskie</span>";
            break;
        case 2:
            player.StartPoint.x = 0;
            player.StartPoint.y = 6;
            textMessage += "<span style='color:#ff0b0a;'>Czerwone</span>";
            break;
        case 3:
            player.StartPoint.x = 10;
            player.StartPoint.y = 4;
            textMessage += "<span style='color:#ffff0f;'>Żółte</span>";
            break;
        case 4:
            player.StartPoint.x = 6;
            player.StartPoint.y = 10;
            textMessage += "<span style='color:#09f241;'>Zielone</span>";
            break;
        default:
            break;
    }
    setPlayerPawns(color, ip);
    return textMessage;
}

function rollTheDice(ip) {
    var player = getPlayerObject(ip);
    var playerPawns = player.pawns;
    var pawnsOnBoard;

    if (player.color !== null) {
        var number = Math.floor(Math.random() * 6) + 1;
        playerPawns.forEach(function (pawn, key) {
            if (pawn.name === "PawnBoard")
                pawnsOnBoard = true; //TODO: PAMIETAJ ABY ZMIENIC NA FALSE GDY WSZYSTKIE PIONKI SA SPOWROTEM NA STARCIE
        });
        if (isPlayerTurn(ip)) {
            if (rolledNumber === 0) {
                rolledNumber = number;
                if (!pawnsOnBoard && rolledNumber !== 6) {
                    endMove();
                    return false;
                } else {
                    return rolledNumber;
                }
            } else {
                return rolledNumber;
            }
        } else {
            return "notYourTurn";
        }
    }
}

function setPlayerPawns(color, ip) {
    var player = getPlayerObject(ip);

    for (var i = 0; i < board.length; i++) {
        for (var q = 0; q < board[i].length; q++) {
            if (typeof (board[i][q]) !== "number") {
                if (board[i][q].color === color) {
                    player.pawns.push(board[i][q]);
                }
            }
        }
    }
}

function sendTurnInfo() {
    io.emit('turnInfo', {playerTurn: playerTurn, rolledNumber: rolledNumber});
}

function findPath(pawn, playerStartPoint) {
    var x = pawn.boardPos[0];
    var y = pawn.boardPos[1];
    var pawnDir = pawn.direction;

    if (pawn.name === "PawnBoard") {
        for (var i = 0; i < rolledNumber; i++) {

            switch (pawnDir) {
                case 'N':
                    if (y + 1 <= 10 && (board[x][y + 1] === 1 || typeof (board[x][y + 1]) == 'object')) {
                        y++;
                    } else if (x - 1 >= 0 && (board[x - 1][y] === 1 || typeof (board[x - 1][y]) == 'object')) {
                        pawnDir = 'W';
                        i--;
                    } else {
                        pawnDir = 'E';
                        i--;
                    }
                    break;
                case 'S':
                    if (y - 1 >= 0 && (board[x][y - 1] === 1 || typeof (board[x][y - 1]) == 'object')) {
                        y--;
                    } else if (x - 1 >= 0 && (board[x - 1][y] === 1 || typeof (board[x - 1][y]) == 'object')) {
                        pawnDir = 'W';
                        i--;
                    } else {
                        pawnDir = 'E';
                        i--;
                    }
                    break;
                case 'W':
                    if (x - 1 >= 0 && (board[x - 1][y] === 1 || typeof (board[x - 1][y]) == 'object')) {
                        x--;
                    } else if (y + 1 <= 10 && (board[x][y + 1] === 1 || typeof (board[x][y + 1]) == 'object')) {
                        pawnDir = 'N';
                        i--;
                    } else {
                        pawnDir = 'S';
                        i--;
                    }
                    break;
                case 'E':
                    if (x + 1 <= 10 && (board[x + 1][y] === 1 || typeof (board[x + 1][y]) == 'object')) {
                        x++;
                    } else if (y + 1 <= 10 && (board[x][y + 1] === 1 || typeof (board[x][y + 1]) == 'object')) {
                        pawnDir = 'N';
                        i--;
                    } else {
                        pawnDir = 'S';
                        i--;
                    }
                    break;
                default:
                    break;
            }
        }
        return [x, y, pawnDir];
    }else if(rolledNumber === 6){
        return [playerStartPoint.x, playerStartPoint.y];
    }else{
        return false;
    }
}

function getPawnById(id) {
    var pawn;

    pawns.forEach(function(item) {
        if(item.id === id)
            pawn = item;
    });

    return pawn;
}

function getPlayerObject(ip) {
    var player = null;

    players.forEach(function (item) {
        if (item.ip === ip) {
            player = item;
        }
    });
    return player;
}

function findPlayerByColor(color) {
    var player;

    players.forEach(function (item) {
        if (item.color === color) {
            player = item;
        }
    });

    return player;
}

function playerInGame(ip) {
    var inGame = false;
    var player = getPlayerObject(ip);
    if (player !== null && player.ip === ip)
        inGame = true;

    return inGame;
}

function isPlayerTurn(ip) {
    var player = getPlayerObject(ip);
    return playerTurn === player.color;
}

function getPawns() {
    return pawns;
}

function endMove() {
    var player = findPlayerByColor(playerTurn);

    if (players.indexOf(player) + 1 < players.length) {
        playerTurn = players[players.indexOf(player) + 1].color;
    } else {
        playerTurn = players[0].color;
    }

    console.log("Tt's " + COLORS[playerTurn] + " turn");
    rolledNumber = 0;

    sendTurnInfo();
}