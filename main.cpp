#include"server.hpp"
using namespace boost::asio;
int main()
{
    try
    {
        Server::getInstance(8080).run(); // utworz i uruchom serwer na porcie 8000
    }
    catch(std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    Server::deleteInstance(); // usun serwer
    return 0;
}
