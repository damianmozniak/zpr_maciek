#include"cstring_exception.hpp"
cstring_exception::cstring_exception(const char* w) : what_(w)
{
}
const char* cstring_exception::what() const noexcept
{
    return what_;
}
