var LUDO = {
    BLUE: 1,
    RED: 2,
    YELLOW: 3,
    GREEN: 4
};

var annotations = $('#annotations');

function showAnnotation(text, time) {
    if (!annotations.hasClass('display')) {
        if (typeof(time) === 'undefined') time = 1000;
        if (typeof(text) !== 'undefined')
            annotations.html(text.replace(/\n/g, '<br />'));

        annotations.addClass('display');
        setTimeout(function () {
            annotations.removeClass('display');
        }, time);
    }
}

/** @type LUDO.BoardController */
var boardController = null;

LUDO.Game = function (options) {
    'use strict';

    options = options || {};

    this.started = false;

    /**********************************************************************************************/
    /* Private methods ****************************************************************************/
    /**
     * Initializer.
     */
    function init() {
        boardController = new LUDO.BoardController({
            containerEl: options.containerEl,
            assetsUrl: options.assetsUrl
        });

        boardController.drawBoard(boardController.setPawnsOnBoard);

        //showAnnotation(); //ABY ZACZAC GRE WYBIERZ PIONKI....
    }
    init();
    getInfo();
};

$(function() {
    $(document).keydown(function(evt) {
        if (LUDO.Game.started && evt.keyCode == 32) {
            rollTheDice();
        }
    });
});

var FizzyText = function () {

    this.Losuj = function () {
        rollTheDice();
    };

    this.startGame = function () {
        socket.emit('startGame');
    };

    this.join = function () {
        socket.emit('joinPlayer');
        gui.remove(joinButton);
        getInfo();
        showAnnotation("Dołącześ do gry\nWybierz swoje pionki");
    };

    this.player = function () {};
};

var gui;
var text;
var joinButton;
var startGameButton;

window.onload = function () {
    gui = new dat.GUI();
    text = new FizzyText();
};

const socket = io.connect(window.location.href);

socket.on('connect', function () {
    console.log('Successfully connected!');
});

var tempLabels = [];

socket.on('playerJoined', function (data) {
    showAnnotation("Dołączył gracz " + data.players, 1000);

    tempLabels.push(gui.add(text, "player").name("Gracz " + data.players));
});

socket.on('gameStarted', function (started) {
    if (started) {
        showAnnotation("Gra rozpoczęta !\nZaczyna Gracz 1", 1000);
        gui.remove(startGameButton);
        getInfo();
    } else {
        showAnnotation("Nie wszyscy wybrali pionki\nProszę czekać");
    }
});

function rollTheDice(){
    socket.emit('rollTheDice', {}, function(rolledNumber){
        boardController.rollTheDice(rolledNumber);
    });
}

function getInfo() {
    socket.emit('getGameInfo', {}, function (gameStarted, playerInGame, numOfPlayers, playerObject) {
        tempLabels.forEach(function (label) {
            gui.remove(label);
        });

        tempLabels = [];
        if (!gameStarted) {
            if (playerInGame)
                startGameButton = gui.add(text, 'startGame').name('Zacznij grę');
            if (!playerInGame)
                joinButton = gui.add(text, "join").name("Dołącz");

        } else {
            tempLabels.push(gui.add(text, "Losuj"));
            LUDO.Game.started = true;

            if(boardController.setIsPlayerTurn())
                boardController.setRolledNumber();

        }

        if (playerInGame) {
            boardController.inGame = true;
            boardController.playerColor = playerObject.color;
        }

        for (var i = 1; i <= numOfPlayers; i++)
            tempLabels.push(gui.add(text, "player").name("Gracz " + i));

    });
}

