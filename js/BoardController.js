LUDO.BoardController = function (options) {
    'use strict';

    options = options || {};

    /**********************************************************************************************/
    /* Private properties *************************************************************************/

    /**
     * The DOM Element in which the drawing will happen.
     * @type HTMLDivElement
     */
    var containerEl = options.containerEl || null;

    /** @type String */
    var assetsUrl = options.assetsUrl || '';

    /** @type THREE.WebGLRenderer */
    var renderer;

    /** @type THREE.Scene */
    var scene;

    /** @type THREE.PerspectiveCamera */
    var camera;

    /** @type THREE.OrbitControls */
    var cameraController;

    /** @type Object */
    var lights = {};

    /** @type Object */
    var materials = {};

    /** @type THREE.Geometry */
    var pieceGeometry = null;

    /** @type THREE.Mesh */
    var boardModel;

    /** @type THREE.Mesh */
    var groundModel;

    var textureLoader;
    //TEXTURES
    var boardTexture;
    var lightSquareTexture;
    var darkSquareTexture;
    var groundTexture;
    var pieceShadowTexture;
    var particlesRing;
    var INTERSECTED = null;

    /**
     * The board square size.
     * @type Number
     * @constant
     */
    var squareSize = 7.28;

    var pickedPawn = null;

    var yourTurn = false;

    var rolledNumber = 0;

    var foundPawn;

    socket.on('turnInfo', function (data) {
        yourTurn = self.playerColor === data.playerTurn;
        rolledNumber = data.rolledNumber;
        console.log('Turn Info updated');
    });

    /**********************************************************************************************/
    /* Public vars *****************************************************************************/
    this.inGame = false;

    this.playerColor = null;

    /**********************************************************************************************/
    /* Public methods *****************************************************************************/

    /**
     * Draws the board.
     */
    this.drawBoard = function (callback) {
        initEngine();
        initLights();
        initTextures();
        initMaterials();

        initObjects(function () {
            onAnimationFrame();

            callback();
        });
        //devTest();
    };

    this.rollTheDice = function (num) {
        if (!num) {
            showAnnotation("Musisz wylosować 6, aby wyjść \nMoże w następnej kolejce Ci się poszczęści", 1500);
        } else if (typeof num === 'number') {
            rolledNumber = num;
            showAnnotation("Wylosowałeś " + num + "\nWybierz pionka, którym chcesz się ruszyć.");
        } else {
            showAnnotation("To nie jest Twoja tura\nPoczekaj na swoją kolej");
        }
    };


    this.setPawnsOnBoard = function () {
        socket.emit('getPawns', {}, function (pawns) {

            pawns.forEach(function (pawn) {
                var pieceMesh = new THREE.Mesh(pieceGeometry);
                var pieceObjGroup = new THREE.Object3D();

                if (pawn.color === LUDO.BLUE) {
                    pieceObjGroup.color = LUDO.BLUE;
                    pieceMesh.material = materials.bluePieceMaterial;
                } else if (pawn.color === LUDO.RED) {
                    pieceObjGroup.color = LUDO.RED;
                    pieceMesh.material = materials.redPieceMaterial;
                } else if (pawn.color === LUDO.YELLOW) {
                    pieceObjGroup.color = LUDO.YELLOW;
                    pieceMesh.material = materials.yellowPieceMaterial;
                } else {
                    pieceObjGroup.color = LUDO.GREEN;
                    pieceMesh.material = materials.greenPieceMaterial;
                }

                var shadowPlane = new THREE.Mesh(new THREE.PlaneGeometry(squareSize, squareSize, 1, 1), materials.pieceShadowPlane);
                shadowPlane.rotation.x = -90 * Math.PI / 180;
                shadowPlane.scale.x = 0.6;
                shadowPlane.scale.y = 0.6;

                pieceObjGroup.add(pieceMesh);
                pieceObjGroup.add(shadowPlane);
                pieceObjGroup.name = "Pawn";
                pieceObjGroup.boardId = pawn.id;
                pieceObjGroup.position.setX(boardToWorld(pawn.boardPos).x);
                pieceObjGroup.position.setZ(boardToWorld(pawn.boardPos).z);

                scene.add(pieceObjGroup);
            });
        });
    };

    /**********************************************************************************************/
    /* Private methods ****************************************************************************/

    /**
     * Initialize some basic 3D engine elements.
     */
    function initEngine() {
        var viewWidth = containerEl.offsetWidth;
        var viewHeight = containerEl.offsetHeight;

        renderer = new THREE.WebGLRenderer({
            antialias: true
        });

        renderer.setSize(viewWidth, viewHeight);
        renderer.setClearColor(0xfefefe, 1);
        scene = new THREE.Scene();

        camera = new THREE.PerspectiveCamera(35, viewWidth / viewHeight, 1, 1000);
        camera.position.set(squareSize * 4, 120, 150);
        cameraController = new THREE.OrbitControls(camera, containerEl);
        cameraController.center = new THREE.Vector3(squareSize * 5.5, 0, squareSize * 5.5);
        scene.add(camera);
        containerEl.appendChild(renderer.domElement);

    }

    /**
     * Initialize the lights.
     */
    function initLights() {
        // top light
        lights.topLight = new THREE.PointLight();
        lights.topLight.position.set(squareSize * 4, 150, squareSize * 4);
        lights.topLight.intensity = 0.4;

        // white's side light
        lights.whiteSideLight = new THREE.SpotLight();
        lights.whiteSideLight.position.set(squareSize * 4, 100, squareSize * 4 + 200);
        lights.whiteSideLight.intensity = 0.8;
        lights.whiteSideLight.shadow.camera.fov = 55;

        // black's side light
        lights.blackSideLight = new THREE.SpotLight();
        lights.blackSideLight.position.set(squareSize * 4, 100, squareSize * 4 - 200);
        lights.blackSideLight.intensity = 0.8;
        lights.blackSideLight.shadow.camera.fov = 55;

        // light that will follow the camera position
        lights.movingLight = new THREE.PointLight(0xf9edc9);
        lights.movingLight.position.set(0, 10, 0);
        lights.movingLight.intensity = 0.5;
        lights.movingLight.distance = 500;

        // add the lights in the scene
        scene.add(lights.topLight);
        scene.add(lights.whiteSideLight);
        scene.add(lights.blackSideLight);
        scene.add(lights.movingLight);
    }

    /**
     * Initialize textures.
     */
    function initTextures() {
        textureLoader = new THREE.TextureLoader();
        boardTexture = textureLoader.load(assetsUrl + 'board_ludo_texture.jpg');
        lightSquareTexture = textureLoader.load(assetsUrl + 'square_light_texture.jpg');
        darkSquareTexture = textureLoader.load(assetsUrl + 'square_dark_texture.jpg');
        groundTexture = textureLoader.load(assetsUrl + 'ground.png');
        pieceShadowTexture = textureLoader.load(assetsUrl + 'piece_shadow.png')
    }

    /**
     * Initialize the materials.
     */
    function initMaterials() {
        // board material
        materials.boardMaterial = new THREE.MeshLambertMaterial({
            map: boardTexture
        });

        // ground material
        materials.groundMaterial = new THREE.MeshBasicMaterial({
            transparent: true,
            map: groundTexture
        });

        // dark square material
        materials.darkSquareMaterial = new THREE.MeshLambertMaterial({
            map: darkSquareTexture
        });
        //
        // light square material
        materials.lightSquareMaterial = new THREE.MeshLambertMaterial({
            map: lightSquareTexture
        });

        // red piece material
        materials.redPieceMaterial = new THREE.MeshPhongMaterial({
            color: 0xff0000,
            shininess: 20
        });

        // blue piece material
        materials.bluePieceMaterial = new THREE.MeshPhongMaterial({
            color: 0x3a47e4,
            shininess: 20
        });


        // yellow piece material
        materials.yellowPieceMaterial = new THREE.MeshPhongMaterial({
            color: 0xffff00,
            shininess: 20
        });

        // green piece material
        materials.greenPieceMaterial = new THREE.MeshPhongMaterial({
            color: 0x00ff40,
            shininess: 20
        });

        // pieces shadow plane material
        materials.pieceShadowPlane = new THREE.MeshBasicMaterial({
            transparent: true,
            map: pieceShadowTexture
        });
    }

    /**
     * Initialize the objects.
     * @param {Object} callback Function to call when the objects have been loaded.
     */
    function initObjects(callback) {
        var loader = new THREE.JSONLoader();
        var totalObjectsToLoad = 2; // board + the piece
        var loadedObjects = 0; // count the loaded pieces

        // checks if all the objects have been loaded
        function checkLoad() {
            loadedObjects++;

            if (loadedObjects === totalObjectsToLoad && callback) {
                callback();
            }
        }

        // load board
        loader.load(assetsUrl + 'board.js', function (geom) {
            boardModel = new THREE.Mesh(geom, materials.boardMaterial);
            boardModel.position.y = -0.02;

            scene.add(boardModel);

            checkLoad();
        });

        // load piece
        loader.load(assetsUrl + 'Pawn.js', function (geometry) {
            pieceGeometry = geometry;
            checkLoad();
        });

        // add ground
        groundModel = new THREE.Mesh(new THREE.PlaneGeometry(100, 100, 1, 1), materials.groundMaterial);
        groundModel.position.set(squareSize * 5.5, -1.52, squareSize * 5.5);
        groundModel.rotation.x = -90 * Math.PI / 180;

        scene.add(groundModel);


        //particlesRing
        particlesRing = new THREE.Object3D();
        particlesRing.position.set(boardToWorld([5, 5]).x, 1, boardToWorld([5, 5]).z);
        particlesRing.visible = false;

        scene.add(particlesRing);

        var material = new THREE.SpriteMaterial({
            map: new THREE.CanvasTexture(generateSprite()),
            blending: THREE.AdditiveBlending
        });

        function initParticleRotation(particlesRing) {
            var tween = new TWEEN.Tween(particlesRing.rotation)
                .to({y: 2 * Math.PI}, 3000)
                .onComplete(function () {
                    particlesRing.rotation.y = 0;
                })
                .start();
            tween.chain(tween);
        }

        function generateSprite() {
            var canvas = document.createElement('canvas');
            canvas.width = 16;
            canvas.height = 16;
            var context = canvas.getContext('2d');
            var gradient = context.createRadialGradient(canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2);

            gradient.addColorStop(0, 'rgba(255,255,255,1)');
            gradient.addColorStop(0.2, 'rgba(0,255,255,1)');
            gradient.addColorStop(0.4, 'rgba(0,0,64,1)');
            gradient.addColorStop(1, 'rgba(0,0,0,1)');
            context.fillStyle = gradient;
            context.fillRect(0, 0, canvas.width, canvas.height);
            return canvas;
        }

        var ileCzasteczek = 25;

        for (var i = 1; i <= ileCzasteczek; i++) {
            var particle = new THREE.Object3D();
            particle.rotation.y = i * (2 * Math.PI / ileCzasteczek);
            particle.translateZ(2);
            particlesRing.add(particle);

            var sprite = new THREE.Sprite(material);
            sprite.translateZ(1);
            sprite.scale.x = sprite.scale.y = Math.random() + 1;
            particle.add(sprite);
        }
        initParticleRotation(particlesRing);

    }


    /**
     * The render loop.
     */
    function onAnimationFrame() {
        requestAnimationFrame(onAnimationFrame);

        cameraController.update();

        // update moving light position
        lights.movingLight.position.x = camera.position.x;
        lights.movingLight.position.z = camera.position.z;

        TWEEN.update();
        renderer.render(scene, camera);
    }

    /**
     * Converts the board position to 3D world position.
     * @param {Array} pos The board position.
     * @returns {THREE.Vector3}
     */
    function boardToWorld(pos) {
        var x = (1 + pos[1]) * squareSize - squareSize / 2;
        var z = (1 + pos[0]) * squareSize - squareSize / 2;

        return new THREE.Vector3(x, 0, z);
    }

    //MOUSE INTERACT
    document.addEventListener('mousedown', onDocumentMouseDown, false);
    document.addEventListener('mousemove', onDocumentMouseMove, false);


    var raycaster = new THREE.Raycaster();
    var mouse = new THREE.Vector3();
    mouse.z = 0.5;
    var self = this;

    function onDocumentMouseMove(event) {
        event.preventDefault();

        mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = -( event.clientY / window.innerHeight ) * 2 + 1;

        if (LUDO.Game.started && self.inGame && yourTurn && rolledNumber !== 0) {
            raycaster.setFromCamera(mouse, camera);
            var intersects = raycaster.intersectObjects(scene.children, true);

            if (intersects.length > 0) {
                pickedPawn = intersects[0].object.parent;
                //JEZELI NAMIERZONO PIONEK
                if (pickedPawn.name === "Pawn") {
                    //JEZELI TO TWOJ PIONEK
                    if (pickedPawn.color === self.playerColor) {
                        //JEZELI TO NIE JEST OSTANO NAMIERZONY
                        if ( pickedPawn != INTERSECTED ) {
                            INTERSECTED = pickedPawn;

                            socket.emit('getPawnPath', pickedPawn.boardId, function(destination){
                                if(destination !== false)
                                    moveParticlesRing(true, destination[0], destination[1]);
                            });
                        }
                    }
                }else{
                    if ( pickedPawn != INTERSECTED ) {
                        INTERSECTED = pickedPawn;
                        moveParticlesRing(false);
                    }
                }
            }
        }
    }

    function onDocumentMouseDown(event) {
        event.preventDefault();

        socket.emit('playerInGame', {}, function (inGame) {
            self.inGame = inGame;
        });

        if (self.inGame) {
            raycaster.setFromCamera(mouse, camera);

            var intersects = raycaster.intersectObjects(scene.children, true);
            if (intersects.length > 0) {
                pickedPawn = intersects[0].object.parent;


                //JEZELI KNIKNIETO PIONEK
                if (pickedPawn.name === "Pawn") {

                    //WYBIERANIE PIONKOW -JEZELI JESZCZE GRACZ NIE MA PRZYPISANEGO KOLORU
                    if (self.playerColor === null) {
                        var pickedColor = pickedPawn.color;

                        socket.emit('colorIsAvailable', {color: pickedColor}, function (available) {
                            if (available) {
                                self.playerColor = pickedColor;
                                setPlayerData(pickedColor);
                            } else {
                                showAnnotation("Ten kolor jest zajęty !\nWybierz inny", 2000);
                            }
                        });
                    }
                    console.log(pickedPawn);
                    if (pickedPawn.color === self.playerColor) {
                        if (yourTurn) {
                            //PROBA RUCHU PRZY TURZE BEZ WYLOSOWANIA CYFRY
                            if (rolledNumber === 0) {
                                showAnnotation("Najpierw wylosuj cyfrę !");
                            } else {
                                socket.emit('movePawn', pickedPawn.boardId, function (msg) {
                                    if (msg)
                                        showAnnotation(msg);
                                });
                            }
                        } else if (!LUDO.Game.started) {
                            showAnnotation("Gra się jeszcze nie rozpoczęła");
                        } else {
                            showAnnotation("To nie jest Twoja tura\nPoczekaj na swoją kolej");
                        }
                    }
                }
            }
        }
    }

    socket.on('movePawn', function (data) {
        var pickedPawn = findPawn(data.pawnId);

        if (data.result === true) {
            movePawn(pickedPawn, data.desireX, data.desireY);
            console.log('Moving pawn color ');
            console.log(pickedPawn.color);
            console.log('to: ' + data.desireX + ' ' + data.desireY);
        } else if (data.result === false) {
            showAnnotation(data.msg);
        }
    });

    function moveParticlesRing(visible, x, y) {
        particlesRing.visible = visible;

        if(typeof x !== 'undefined') {
            particlesRing.position.set(boardToWorld([x, y]).x, 1, boardToWorld([x, y]).z);
        }
    }

    function findPawn(id) {
        scene.children.forEach(function (pawn) {
            if (pawn.name === "Pawn")
                if (pawn.boardId === id)
                    foundPawn = pawn;
        });

        return foundPawn;
    }

    function setPlayerData(color) {
        socket.emit('setPlayerData', color, function (msg) {
            showAnnotation(msg, 2000);
        });
    }

    function movePawn(pawn, x, y) {
        pawn.position.z = (x + 1) * squareSize - squareSize / 2;
        pawn.position.x = (y + 1) * squareSize - squareSize / 2;
        moveParticlesRing(false);
    }

    this.setIsPlayerTurn = function () {
        var x;

        socket.emit('isPlayerTurn', {}, function (isPlayerTurn) {
            x = isPlayerTurn;
            yourTurn = isPlayerTurn;
        });

        return x;
    };

    this.setRolledNumber = function () {
        socket.emit('getRolledNumber', {}, function (number) {
            rolledNumber = number;
        });


        return rolledNumber;
    };

};
