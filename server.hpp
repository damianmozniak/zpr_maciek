#ifndef SERVER_HPP_INCLUDED
#define SERVER_HPP_INCLUDED
#include<boost/asio.hpp>
#include<boost/bind/bind.hpp>
#include<iostream>
#include<string>
#include<fstream>
#include<list>
#include<map>
#include<sstream>
#include<utility>
#include<mutex>
#include<signal.h>
#include<time.h>
#include"cstring_exception.hpp"
using namespace boost::asio;
std::string to_string(const int& arg);
std::string pad(const std::string& str, const char& dlm, const size_t& dstlen);
std::string mimeType(const std::string& ext);
const size_t RECV_BUF_SIZE = 8192;
class Server // klasa reprezentujaca serwer gry sieciowej (wykorzystujaca wzorzec singletona)
{
    static Server* instance;
    io_service ios;
    ip::tcp::endpoint* endp;
    ip::tcp::acceptor* accr;
    std::list<ip::tcp::socket> conns; // lista otwartych polaczen z klientami
    std::mutex conns_mutex;
    ip::tcp::socket sock;
    std::string htdoc;
    std::string mime_type;
    unsigned conns_nt;
    void conns_enter();//
    void conns_leave();//
    Server(unsigned short port);
    Server(const Server&) = delete;
    Server& operator=(const Server&) = delete;
    ~Server();
    std::string serverTime();
    std::string HTTPResponse(const unsigned short& code);
    bool prepareFile(const std::string& fname);
    void startAccepting();
    void handleAccept(/*std::list<ip::tcp::socket>::iterator sock_it, */const boost::system::error_code& error);
    void handleWrite(std::list<ip::tcp::socket>::iterator sock_it, const boost::system::error_code& error, size_t s);
    void handleReceive(std::list<ip::tcp::socket>::iterator sock_it, char* buf, const boost::system::error_code& error, size_t s);
    void receive(std::list<ip::tcp::socket>::iterator sock_it);
public:
    static Server& getInstance(unsigned short port);
    static void deleteInstance();
    void run();
};
#endif // SERVER_HPP_INCLUDED
