#include"server.hpp"
std::string to_string(const int& arg)
{
    std::stringstream s;
    s << arg;
    return s.str();
}
std::string pad(const std::string& str, const char& dlm, const size_t& dstlen)
{
    std::string res = str;
    while(res.size()<dstlen)
        res = dlm+res;
    return res;
}
std::string mimeType(const std::string& ext)
{
    if(ext=="html")
        return "text/html; charset=utf-8";//"application/xhtml+xml; charset=utf-8";
    if(ext=="css")
        return "text/css";
    if(ext=="js")
        return "application/javascript";
    if(ext=="ttf")
        return "font/ttf";
    return "text/plain";
}
Server* Server::instance = nullptr;
Server& Server::getInstance(unsigned short port = 0) // przy pierwszym wywolaniu tej funkcji trzeba podac wiekszy od 0 numer portu, na ktorym serwer ma zostac uruchomiony
{
    if(!instance)
    {
        if(!port)
            throw cstring_exception("Nie zostal podany numer portu serwera lub jest on rowny 0.");
        instance = new Server(port);
    }
    return *instance;
}
void Server::deleteInstance()
{
    if(instance)
        delete instance;
}
Server::Server(unsigned short port) : sock(ip::tcp::socket(ios)), conns_nt(0)
{
    endp = new ip::tcp::endpoint(ip::tcp::v4(), port);
    accr = new ip::tcp::acceptor(ios, *endp);
}
bool Server::prepareFile(const std::string& fname)
{
    htdoc.clear();
    std::ifstream in(fname, std::ifstream::in | std::ifstream::binary);
    if(in.fail())
    {
        /*std::string es = "Nie udalo sie otworzyc pliku o sciezce `";
        es += fname;
        es += "`. Byc moze ten plik nie istnieje.";
        throw cstring_exception(es.c_str());*/
        return false;
    }
    htdoc = "";
    char t;
    while(!in.eof())
    {
        in.get(t);
        htdoc += t;
    }
    in.close();
    std::string ext;
    size_t i = fname.size()-1;
    while((fname[i]!='.') && (fname[i]!='/') && (i!=0))
    {
        ext = fname[i]+ext;
        i--;
    }
    if((fname[i]=='/') || ((i==0) && (fname[i]!='.')))
        ext = "";
    mime_type = mimeType(ext);
    return true;
}
std::string Server::HTTPResponse(const unsigned short& code = 200)
{
    if(code==200)
    {
        std::string resp = "HTTP/1.1 200 OK\n";
        std::list<std::pair<std::string, std::string> > values;
        values.push_back(std::make_pair("Cache-Control", "no-cache"));
        values.push_back(std::make_pair("Connection", "Keep-Alive"));
        values.push_back(std::make_pair("Transfer-Encoding", "identity"));
        values.push_back(std::make_pair("Content-Type", mime_type));
        values.push_back(std::make_pair("Content-Length", to_string(htdoc.size())));
        //values.push_back(std::make_pair("Keep-Alive", "timeout=30"));
        values.push_back(std::make_pair("Date", serverTime()));
        for(std::list<std::pair<std::string, std::string> >::iterator it = values.begin(); it!=values.end(); ++it)
            resp += (it->first+": "+it->second+'\n');
        resp += '\n';
        return resp;
    }
    if(code==404)
        return "HTTP/1.1 404 Not Found\n\n";
    return "";
}
const char* monthNames[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
const char* weekdayNames[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
std::string Server::serverTime()
{
    time_t t1;
    time(&t1);
    struct tm* st = gmtime(&t1);
    std::string res = " GMT";
    res = pad(to_string(st->tm_sec), '0', 2)+res;
    res = pad(to_string(st->tm_min), '0', 2)+':'+res;
    res = pad(to_string(st->tm_hour), '0', 2)+':'+res;
    res = to_string(st->tm_year+1900)+' '+res;
    res = std::string(monthNames[st->tm_mon])+' '+res;
    res = pad(to_string(st->tm_mday), '0', 2)+' '+res;
    res = std::string(weekdayNames[st->tm_wday])+", "+res;
    return res;
}
Server::~Server()
{
    if(accr->is_open()) // w razie sytuacji, ktora wystepuje po niektorych zgloszeniach wyjatkow w niniejszej aplikacji
        accr->close();
    //conns_enter();
    while(!conns.empty()) // w razie sytuacji, ktora wystepuje po niektorych zgloszeniach wyjatkow w niniejszej aplikacji
    {
        conns.back().close();
        conns.pop_back();
    }
    //conns_leave();
    if(sock.is_open())
    {
        sock.close();
    }
    delete accr;
    delete endp;
}
void Server::handleWrite(std::list<ip::tcp::socket>::iterator sock_it, const boost::system::error_code& error, size_t s)
{
    if(error)
        if(error==boost::asio::error::operation_aborted)
        {
            throw cstring_exception("Nieudane wyslanie pliku do klienta.");
        }
        else
        {
            //conns_enter();
            sock_it->close();
            conns.erase(sock_it);
            //conns_leave();
        }
    else {}
        /*receive(sock_it);*/
}
void Server::handleAccept(/*std::list<ip::tcp::socket>::iterator sock_it, */const boost::system::error_code& error)
{
    if(!accr->is_open()) // jesli zamykamy program serwera
        return;
    //conns_enter();
    startAccepting();
    //conns_leave();
    if(error) {}
        //throw cstring_exception("Nieudane nawiazanie polaczenia z klientem.");
    else
    {
        conns.push_back(std::move(sock));
        std::list<ip::tcp::socket>::iterator sock_it = conns.end();
        receive(--sock_it);
    }
}
void Server::startAccepting() // funkcja inicjujaca nowe polaczenie, dodajaca je przy tym do listy otwartych polaczen
{
    /*conns.push_back(std::move(sock));
    std::list<ip::tcp::socket>::iterator sock_it = conns.end();
    sock_it--;*/
    accr->async_accept(sock, boost::bind(&Server::handleAccept, this, /*sock_it, */placeholders::error));
}
void Server::run() // uruchomienie serwera
{
    signal_set signals(ios); // zestaw sygnalow, ktore beda obslugiwane przez program
    signals.add(SIGINT);
    signals.add(SIGTERM);
#if defined(SIGQUIT)
    signals.add(SIGQUIT);
#endif // defined(SIGQUIT)
    signals.async_wait(
        [this](boost::system::error_code error, int signo)
        { // ponizsza procedura jest uruchamiana, gdy zamykamy program serwera (na przyklad przy nacisnieciu Ctrl+C)
            accr->close();
            //conns_enter();
            while(!conns.empty())
            {
                conns.back().close();
                conns.pop_back();
            }
            //conns_leave();
            sock.close();
        });
    accr->listen();
    //conns_enter();
    startAccepting();
    //conns_leave();
    ios.run();
}
void Server::handleReceive(std::list<ip::tcp::socket>::iterator sock_it, char* buf, const boost::system::error_code& error, size_t s)
{
    //conns_enter();
    if(error)
    {
        if(error==boost::asio::error::operation_aborted)
        {
            //std::cout << sock_it->second.size() << '\n';
        }
            /*throw cstring_exception("Nieudane odebranie wiadomosci HTTP od klienta.");*/
        else
        {
            //conns_enter();
            sock_it->close();
            conns.erase(sock_it);
            //conns_leave();
        }
    }
    else
    {
        receive(sock_it);
        if(s<RECV_BUF_SIZE)
            buf[s] = '\0';
        std::string req_path;
        for(size_t i = 5; buf[i]!=' '; ++i)
            if(buf[i]!='%')
                req_path += buf[i];
            else
            {
                req_path += '\0';
                for(unsigned j = 0; j<2; ++j)
                {
                    i++;
                    if((buf[i]>='0') && (buf[i]<='9'))
                        req_path[req_path.size()-1] += (buf[i]-'0')*((j==0) ? 16 : 1);
                    else
                        req_path[req_path.size()-1] += (buf[i]-'A'+10)*((j==0) ? 16 : 1);
                }
            }
        if(req_path=="")
            req_path = "index.html";
        std::string hthead;
        if(!prepareFile(req_path))
        {
            std::cout << "plik " << req_path << " prawdopodobnie nie zostal znaleziony\n";
            hthead = HTTPResponse(404);
        }
        else
        {
            std::cout << "przesylam plik " << req_path << '\n';
            hthead = HTTPResponse();
        }
        async_write(*sock_it, buffer(hthead+htdoc), boost::bind(&Server::handleWrite, this, sock_it, placeholders::error, placeholders::bytes_transferred));
    }
    delete [] buf;
    //conns_leave();
}
void Server::receive(std::list<ip::tcp::socket>::iterator sock_it)
{
    char* buf = new char[RECV_BUF_SIZE];
    sock_it->async_read_some(buffer(buf, RECV_BUF_SIZE), boost::bind(&Server::handleReceive, this, sock_it, buf, placeholders::error, placeholders::bytes_transferred));
}
void Server::conns_enter()
{
    conns_mutex.lock();
    if(conns_nt)
    {
        conns_mutex.unlock();
        throw cstring_exception("Wiecej niz jeden proces w sekcji krytycznej.\n");
    }
    conns_nt++;
}
void Server::conns_leave()
{
    if(conns_nt)
        conns_nt--;
    conns_mutex.unlock();
}
